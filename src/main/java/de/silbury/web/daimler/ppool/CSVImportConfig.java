package de.silbury.web.daimler.ppool;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

// TODO: 5. Create config for CSV import
@EnableBatchProcessing
@Configuration
public class CSVImportConfig {

  @Autowired
  public DataSource dataSource;

  @Autowired
  public StepBuilderFactory stepBuilderFactory;

  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  public static long startTime = 0;

  private String importFilename = "superheroes.csv";

  // TODO: 6. Define CSV config, e.g., column names, column delimeter
  @Bean
  public FlatFileItemReader<Superhero> csvReader() {
    FlatFileItemReader<Superhero> reader = new FlatFileItemReader<>();
    reader.setResource(new ClassPathResource(importFilename));
    reader.setLineMapper(new DefaultLineMapper<Superhero>() {
      {
        setLineTokenizer(new DelimitedLineTokenizer() {
          {
            setNames(
              "id",
              "superheroName",
              "birthName",
              "favoriteColor",
              "powerHeight",
              "employed",
              "field_7",
              "field_8",
              "field_9",
              "field_10",
              "field_11",
              "field_12",
              "field_13",
              "field_14",
              "field_15",
              "field_16",
              "field_17",
              "field_18",
              "field_19",
              "field_20",
              "field_21",
              "field_22",
              "field_23",
              "field_24",
              "field_25",
              "field_26",
              "field_27",
              "field_28",
              "field_29",
              "field_30",
              "field_31",
              "field_32",
              "field_33",
              "field_34",
              "field_35",
              "field_36",
              "field_37",
              "field_38",
              "field_39",
              "field_40"
            );
            setDelimiter(";");
          }
        });

        setFieldSetMapper(new BeanWrapperFieldSetMapper<Superhero>() {
          {
            setTargetType(Superhero.class);
          }
        });
      }
    });

    return reader;
  }

  // TODO: 7. Boilerplate - define your processor
  @Bean
  ItemProcessor<Superhero, Superhero> csvProcessor() {
    return new SuperheroProcessor();
  }

  // TODO: 8. Define DB insert / update
  @Bean
  public JdbcBatchItemWriter<Superhero> dbWriter() {
    JdbcBatchItemWriter<Superhero> writer = new JdbcBatchItemWriter<>();
    writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
    writer.setSql(
      "INSERT INTO superheroes (id, superheroName, birthName, favoriteColor, powerHeight, employed) " +
        "VALUES (:id, :superheroName, :birthName, :favoriteColor, :powerHeight, :employed)"
    );
    writer.setDataSource(dataSource);

    return writer;
  }

  // TODO: 9. Define the writer step and chunk size
  @Bean
  public Step dbWriterStep() {
    return stepBuilderFactory.get("dbWriterStep")
      .<Superhero, Superhero>chunk(1)
      .reader(csvReader())
      .processor(csvProcessor())
      .writer(dbWriter())
      .build();
  }

  // TODO: 10. Boilerplate - define the db writer job with completion listener
  @Bean
  Job dbWriterJob(JobCompletionNotificationListener listener) {
    return jobBuilderFactory.get("dbWriterJob")
      .incrementer(new RunIdIncrementer())
      .listener(listener)
      .flow(dbWriterStep())
      .end()
      .build();
  }

}
