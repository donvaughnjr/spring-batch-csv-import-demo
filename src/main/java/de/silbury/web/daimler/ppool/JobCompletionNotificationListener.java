package de.silbury.web.daimler.ppool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

// TODO: 11. Do something in the import completion listener
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

  private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public JobCompletionNotificationListener(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public void afterJob(JobExecution jobExecution) {
    if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
      double jobDuration = (System.currentTimeMillis() - CSVImportConfig.startTime) / 1000.0;

      log.info(String.format("\n\n============ IMPORT FINISHED in %.2f seconds ============ Verifying the results (showing first 10)....\n", jobDuration));

      List<Superhero> results = jdbcTemplate.query("SELECT id, superheroName, birthName, favoriteColor, powerHeight, employed FROM superheroes LIMIT 10", new RowMapper<Superhero>() {
        @Override
        public Superhero mapRow(ResultSet rs, int row) throws SQLException {
          return new Superhero(
            rs.getInt(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getFloat(5),
            rs.getBoolean(6)
          );
        }
      });

      for (Superhero superhero : results) {
        log.info(String.format("Discovered \"%s\" in the database.", superhero.getSuperheroName()));
      }

      log.info(String.format(
        "\n\n================================================================================================================================================\n",
        jobDuration
      ));
    }
  }

}
