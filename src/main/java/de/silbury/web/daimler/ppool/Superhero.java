package de.silbury.web.daimler.ppool;

// TODO: 2. Create model class
public class Superhero {

  private int id;
  private String superheroName;
  private String birthName;
  private String favoriteColor;
  private float powerHeight;
  private boolean employed;
  private String field_7;
  private String field_8;
  private String field_9;
  private String field_10;
  private String field_11;
  private String field_12;
  private String field_13;
  private String field_14;
  private String field_15;
  private String field_16;
  private String field_17;
  private String field_18;
  private String field_19;
  private String field_20;
  private String field_21;
  private String field_22;
  private String field_23;
  private String field_24;
  private String field_25;
  private String field_26;
  private String field_27;
  private String field_28;
  private String field_29;
  private String field_30;
  private String field_31;
  private String field_32;
  private String field_33;
  private String field_34;
  private String field_35;
  private String field_36;
  private String field_37;
  private String field_38;

  public String getField_7() {
    return field_7;
  }

  public void setField_7(String field_7) {
    this.field_7 = field_7;
  }

  public String getField_8() {
    return field_8;
  }

  public void setField_8(String field_8) {
    this.field_8 = field_8;
  }

  public String getField_9() {
    return field_9;
  }

  public void setField_9(String field_9) {
    this.field_9 = field_9;
  }

  public String getField_10() {
    return field_10;
  }

  public void setField_10(String field_10) {
    this.field_10 = field_10;
  }

  public String getField_11() {
    return field_11;
  }

  public void setField_11(String field_11) {
    this.field_11 = field_11;
  }

  public String getField_12() {
    return field_12;
  }

  public void setField_12(String field_12) {
    this.field_12 = field_12;
  }

  public String getField_13() {
    return field_13;
  }

  public void setField_13(String field_13) {
    this.field_13 = field_13;
  }

  public String getField_14() {
    return field_14;
  }

  public void setField_14(String field_14) {
    this.field_14 = field_14;
  }

  public String getField_15() {
    return field_15;
  }

  public void setField_15(String field_15) {
    this.field_15 = field_15;
  }

  public String getField_16() {
    return field_16;
  }

  public void setField_16(String field_16) {
    this.field_16 = field_16;
  }

  public String getField_17() {
    return field_17;
  }

  public void setField_17(String field_17) {
    this.field_17 = field_17;
  }

  public String getField_18() {
    return field_18;
  }

  public void setField_18(String field_18) {
    this.field_18 = field_18;
  }

  public String getField_19() {
    return field_19;
  }

  public void setField_19(String field_19) {
    this.field_19 = field_19;
  }

  public String getField_20() {
    return field_20;
  }

  public void setField_20(String field_20) {
    this.field_20 = field_20;
  }

  public String getField_21() {
    return field_21;
  }

  public void setField_21(String field_21) {
    this.field_21 = field_21;
  }

  public String getField_22() {
    return field_22;
  }

  public void setField_22(String field_22) {
    this.field_22 = field_22;
  }

  public String getField_23() {
    return field_23;
  }

  public void setField_23(String field_23) {
    this.field_23 = field_23;
  }

  public String getField_24() {
    return field_24;
  }

  public void setField_24(String field_24) {
    this.field_24 = field_24;
  }

  public String getField_25() {
    return field_25;
  }

  public void setField_25(String field_25) {
    this.field_25 = field_25;
  }

  public String getField_26() {
    return field_26;
  }

  public void setField_26(String field_26) {
    this.field_26 = field_26;
  }

  public String getField_27() {
    return field_27;
  }

  public void setField_27(String field_27) {
    this.field_27 = field_27;
  }

  public String getField_28() {
    return field_28;
  }

  public void setField_28(String field_28) {
    this.field_28 = field_28;
  }

  public String getField_29() {
    return field_29;
  }

  public void setField_29(String field_29) {
    this.field_29 = field_29;
  }

  public String getField_30() {
    return field_30;
  }

  public void setField_30(String field_30) {
    this.field_30 = field_30;
  }

  public String getField_31() {
    return field_31;
  }

  public void setField_31(String field_31) {
    this.field_31 = field_31;
  }

  public String getField_32() {
    return field_32;
  }

  public void setField_32(String field_32) {
    this.field_32 = field_32;
  }

  public String getField_33() {
    return field_33;
  }

  public void setField_33(String field_33) {
    this.field_33 = field_33;
  }

  public String getField_34() {
    return field_34;
  }

  public void setField_34(String field_34) {
    this.field_34 = field_34;
  }

  public String getField_35() {
    return field_35;
  }

  public void setField_35(String field_35) {
    this.field_35 = field_35;
  }

  public String getField_36() {
    return field_36;
  }

  public void setField_36(String field_36) {
    this.field_36 = field_36;
  }

  public String getField_37() {
    return field_37;
  }

  public void setField_37(String field_37) {
    this.field_37 = field_37;
  }

  public String getField_38() {
    return field_38;
  }

  public void setField_38(String field_38) {
    this.field_38 = field_38;
  }

  public String getField_39() {
    return field_39;
  }

  public void setField_39(String field_39) {
    this.field_39 = field_39;
  }

  public String getField_40() {
    return field_40;
  }

  public void setField_40(String field_40) {
    this.field_40 = field_40;
  }

  private String field_39;
  private String field_40;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getSuperheroName() {
    return superheroName;
  }

  public void setSuperheroName(String superheroName) {
    this.superheroName = superheroName;
  }

  public String getBirthName() {
    return birthName;
  }

  public void setBirthName(String birthName) {
    this.birthName = birthName;
  }

  public String getFavoriteColor() {
    return favoriteColor;
  }

  public void setFavoriteColor(String favoriteColor) {
    this.favoriteColor = favoriteColor;
  }

  public float getPowerHeight() {
    return powerHeight;
  }

  public void setPowerHeight(float powerHeight) {
    this.powerHeight = powerHeight;
  }

  public boolean isEmployed() {
    return employed;
  }

  public void setEmployed(boolean employed) {
    this.employed = employed;
  }

  public Superhero() {
  }

  public Superhero(
    int id,
    String superheroName,
    String birthName,
    String favoriteColor,
    float powerHeight,
    boolean employed
  ) {
    this.id = id;
    this.superheroName = superheroName;
    this.birthName = birthName;
    this.favoriteColor = favoriteColor;
    this.powerHeight = powerHeight;
    this.employed = employed;
  }

}
