package de.silbury.web.daimler.ppool;

import org.springframework.batch.item.ItemProcessor;

// TODO: 3. Create processor
public class SuperheroProcessor implements ItemProcessor<Superhero, Superhero> {
  @Override
  public Superhero process(final Superhero superhero) throws Exception {
    if (CSVImportConfig.startTime == 0) {
      CSVImportConfig.startTime = System.currentTimeMillis();
    }

  // TODO: 4. Modify CSV values here before storing in DB
    final int id = superhero.getId();
    final String superheroName = superhero.getSuperheroName();
    final String birthName = superhero.getBirthName();
    final String favoriteColor = superhero.getFavoriteColor();
    final float powerHeight = superhero.getPowerHeight();
    final boolean employed = superhero.isEmployed();

    final Superhero processedSuperhero = new Superhero(
      id,
      superheroName,
      birthName,
      favoriteColor,
      powerHeight,
      employed
    );

    return processedSuperhero;
  }
}
